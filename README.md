#Application installation:

Please use php 5.* for easy run

Copy application folder to server.


(1) changes at Application/config  (Please change the base url on the current host)

  $config['base_url'] = 'http://localhost:8888/spaceshipassignment/';

  

(2) database set-up: Please run at mysql

CREATE TABLE `spaceship_assignment` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` text NOT NULL,
  `verification_key` varchar(250) NOT NULL,
  `is_email_verified` enum('no','yes') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `spaceship_assignment`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `spaceship_assignment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



(3) database.php  (edit for your localsettings)(example)  
$db['default'] = array(  
  'dsn' => '',  
  'hostname' => 'localhost',  
  'username' => 'spaceship', //mysql username  
  'password' => 'spaceship', //mysql password  
  'database' => 'spaceship', //created a new database or you can also use any test database in you mysql  
  'dbdriver' => 'mysqli',  
  'dbprefix' => '',  
  'pconnect' => FALSE,  
  'db_debug' => (ENVIRONMENT !== 'production'),  
  'cache_on' => FALSE,  
  'cachedir' => '',  
  'char_set' => 'utf8',  
  'dbcollat' => 'utf8_general_ci',  
  'swap_pre' => '',  
  'encrypt' => FALSE,  
  'compress' => FALSE,  
  'stricton' => FALSE,  
  'failover' => array(),  
  'save_queries' => TRUE  
);  
  

Extra: for info: (my heroku app set-up is also like) (Because I am using mysql cloudDB)  
$db['default'] = array(  
  'dsn' => '',  
  'hostname' => 'us-cdbr-iron-east-02.cleardb.net',  
  'username' => 'badf32b8fc446d',  
  'password' => 'f75b1331',  
  'database' => 'heroku_0f916bc710d6fb0',  
  'dbdriver' => 'mysqli',  
  'dbprefix' => '',  
  'pconnect' => FALSE,  
  'db_debug' => (ENVIRONMENT !== 'production'),  
  'cache_on' => FALSE,  
  'cachedir' => '',  
  'char_set' => 'utf8',  
  'dbcollat' => 'utf8_general_ci',  
  'swap_pre' => '',  
  'encrypt' => FALSE,  
  'compress' => FALSE,  
  'stricton' => FALSE,  
  'failover' => array(),  
  'save_queries' => TRUE  
);  


