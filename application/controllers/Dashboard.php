<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';

// class Private_area extends CI_Controller {
class Dashboard extends API_Controller {

   // public function __construct() {
   //      parent::__construct();
   //  }


 public function __construct()
 {

  parent::__construct();


  //check if token exist

  if(!$this->session->userdata('token'))
  {
   redirect('login');
  } else {

    //get token
    $this->load->library('authorization_token');
    $tokenData = array(
      "Authorization" => $this->session->userdata('token')
    );

    $resultTokenVerification = $this->authorization_token->validateTokenView($tokenData);

    if(!$resultTokenVerification['status'])  {
      logout();
    }
  }
 }

 function user() {
    $tokenData = $this->getToken();
    $query = $this->db->query("SELECT * FROM  spaceship_assignment WHERE email ='". $tokenData['data']->id . "';");
    $data['email'] = '';
    $data['name']='';
    $data['verified']='';

    if (sizeof($query->result()) == 1) {
      $data['email'] = $query->result()[0]->email;
      $data['name'] = $query->result()[0]->name;
      $data['verified']= $query->result()[0]->is_email_verified;
    }
    $this->load->view('userinfo', $data);
 }

 function index()
 {
    // $this->load->helper('url');

    redirect('dashboard/user');
 }


 function users() {
  $queryUsers['users'] = '';
    $queryUsers['users'] = $this->db->query("SELECT * FROM  spaceship_assignment;");
    $this->load->view('userlist', $queryUsers);
 }

 function logout()
 {
  $data = $this->session->all_userdata();
  foreach($data as $row => $rows_value)
  {
   $this->session->unset_userdata($row);
  }
  redirect('login');
 }

 private function getToken() {
    $this->load->library('authorization_token');
    $tokenData = array(
      "Authorization" => $this->session->userdata('token')
    );

    $resultTokenVerification = $this->authorization_token->validateTokenView($tokenData);
    return $resultTokenVerification;
 }
}

?>