<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

 public function __construct()
 {
  parent::__construct();
  if($this->session->userdata('id'))
  {
   redirect('dashboard');
  }
  $this->load->library('form_validation');
  $this->load->library('encrypt');
  $this->load->model('register_model');
 }

 function index()
 {
  $this->load->view('register');
 }

 function validation()
 {
  $this->form_validation->set_rules('user_name', 'Name', 'required|trim');
  $this->form_validation->set_rules('user_email', 'Email Address', 'required|trim|valid_email|is_unique[spaceship_assignment.email]');
  $this->form_validation->set_rules('user_password', 'Password', 'required');
  if($this->form_validation->run())
  {
   $verification_key = md5(rand());
   $encrypted_password = $this->encrypt->encode($this->input->post('user_password'));
   $data = array(
    'name'  => $this->input->post('user_name'),
    'email'  => $this->input->post('user_email'),
    'password' => $encrypted_password,
    'verification_key' => $verification_key
   );
   $id = $this->register_model->insert($data);
   if($id > 0)
   {
    $subject = "Please verify email for login";
    $message = "
    <p>Hi ".$this->input->post('user_name')."</p>
    <p>This is email verification mail from SpaceShip Assignment Login Registration system. To complete registration process and login into system. Please verify by clicking this <a href='".base_url()."register/verify_email/".$verification_key."'>link</a>.</p>
    <p>Once you click this link your email will be verified and you can login into system.</p>
    <p>Thank you.</p>
    ";
    $config = array(
     'protocol'  => 'smtp',
     'smtp_host' => 'ssl://smtp.gmail.com',
     'smtp_port' => 465,
     'smtp_user'  => 'rdtest.t1234@gmail.com', 
     'smtp_pass'  => 'testrd1234!', 
     'mailtype'  => 'html',
     'charset'    => 'iso-8859-1',
     'wordwrap'   => TRUE
    );

    $this->load->library('email', $config);
    $this->email->set_newline("\r\n");
    $this->email->from('info@webslesson.info');
    $this->email->to($this->input->post('user_email'));
    $this->email->subject($subject);
    $this->email->message($message);
    if($this->email->send())
    {
     $this->session->set_flashdata('message', 'Check in your email for email verification mail');
     redirect('register');
    }
   }
  }
  else
  {
   $this->index();
  }
 }

 function verify_email()
 {
  if($this->uri->segment(3))
  {
   $verification_key = $this->uri->segment(3);
   if($this->register_model->verify_email($verification_key))
   {
    // $data['message'] = '<h1 align="center">Your Email has been successfully verified, now you can login from <a href="'.base_url().'login">here</a></h1>';

    $data['message'] = '<div class="alert alert-primary" role="alert"> Your Email has been successfully verified, now you can login from <a href="'.base_url().'login">here</a>! </div>';

   }
   else
   {
    // $data['message'] = '<h1 align="center">Invalid Link</h1>';
    $data['message'] = '<div class="alert alert-danger" role="alert"> Invalid Link </div>';
   }
   $this->load->view('email_verification', $data);
  }
 }

}

?>