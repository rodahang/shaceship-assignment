<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

 public function __construct()
 {
  parent::__construct();
  if($this->session->userdata('id'))
  {
   redirect('dashboard');
  }
  $this->load->library('form_validation');
  $this->load->library('encrypt');
  $this->load->model('login_model');
 }

 function index()
 {
  $this->load->view('login');
 }

 function validation()
 {
  $this->form_validation->set_rules('user_email', 'Email Address', 'required|trim|valid_email');
  $this->form_validation->set_rules('user_password', 'Password', 'required');
  if($this->form_validation->run())
  {
   $result = $this->login_model->can_login($this->input->post('user_email'), $this->input->post('user_password'));
   if($result == '')
   {
      //init token
      $payload = [
            'id' => $this->input->post('user_email'),
            'verified' => "true"
      ];
      // Load Authorization Library or Load in autoload config file
      $this->load->library('authorization_token');
      // generte a token
      $token = $this->authorization_token->generateToken($payload);
      $this->session->set_userdata('token', $token);

      // you user authentication code will go here, you can compare the user with the database or whatever
      redirect('dashboard');
   }
   else
   {
    $this->session->set_flashdata('message',$result);
    redirect('login');
   }
  }
  else
  {
   $this->index();
  }
 }

}

?>
