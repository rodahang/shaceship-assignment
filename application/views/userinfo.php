<!-- <!DOCTYPE html>
<html>
<head>
 <title>User Info</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
</head>

<body>
  
</body>
</html> -->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>SpaceShip Assignment</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url(); ?>css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">

   

</head>

<body>


	<!--Navbar -->
<nav class="mb-1 navbar navbar-expand-lg navbar-dark info-color lighten-1">
  <a class="navbar-brand" href="#">SpaceShip</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555"
    aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-555">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Profile
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>dashboard/users">Signed-up Users</a>
      </li>


       
    </ul>
    <ul class="navbar-nav ml-auto nav-flex-icons">
    	 <li class="nav-item">
      	<a class="nav-link" href="<?php echo base_url(); ?>dashboard/logout">Logout</a>
      </li>
      <li class="nav-item avatar">
        <a class="nav-link p-0" href="#">
          <img src="https://ae01.alicdn.com/kf/HTB1vzC9MVXXXXaPXVXXq6xXFXXXl/13-1-11-7CM-Adorable-Cartoon-Panda-Head-Vinyl-Car-Stickers-Cute-Tail-Car-Styling-Decal.jpg_q50.jpg" class="rounded-circle z-depth-0"
            alt="avatar image" height="35">
        </a>
      </li>
    </ul>
  </div>
</nav>
<!--/.Navbar -->
    


<div style="width: 300px; margin: auto; margin-top: 50px;">
	<!-- Card Wider -->
<div class="card card-cascade wider">

  <!-- Card image -->
  <div class="view view-cascade overlay">
    <img  class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/photo6.jpg" alt="Card image cap">
    <a href="#!">
      <div class="mask rgba-white-slight"></div>
    </a>
  </div>

  <!-- Card content -->
  <div class="card-body card-body-cascade text-center">

    <!-- Title -->
    <h4 class="card-title"><strong><?php
	  echo $name;
  	?></strong></h4>
    <!-- Subtitle -->
    <h5 class="blue-text pb-2"><strong><?php
	  echo $email;
  	?></strong></h5>
    <!-- Text -->
    <p class="card-text">Status: <?php
    	if ($verified) {
    		echo 'verified';
    	} else {
    		echo 'not verified';
    	}
	  
  	?> </p>


  </div>

</div>
<!-- Card Wider -->

 
</div>

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/mdb.min.js"></script>
</body>



</html>



