<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>SpaceShip Assignment</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url(); ?>css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
</head>

<body style="margin-top: 50px;">


 

  <div class="container">

    <h5 class="card-header info-color white-text text-center py-4">
        <strong>Sign up</strong>
    </h5>

    <?php
                    if($this->session->flashdata('message'))
                    {
                        echo '
                        <br><div class="alert alert-success">
                            '.$this->session->flashdata("message").'
                        </div>
                        ';
                    }
                    ?>

    <!--Card content-->
    <div class="card-body px-lg-5 pt-0">

        <!-- Form -->
        <form method="post" class="text-center" style="color: #757575;" action="<?php echo base_url(); ?>register/validation"><br>

            <div class="form-row">
                <div class="col">
                    <!-- First name -->
                    <div class="md-form">
                        <input type="text" name="user_name" id="materialRegisterFormFirstName" class="form-control" value="<?php echo set_value('user_name'); ?>">
                        <label for="materialRegisterFormFirstName">Name</label>
                        <span class="text-danger"><?php echo form_error('user_name'); ?></span>
                    </div>
                </div>
                 
            </div>

            <div class="md-form mt-0">
                <input type="email" name="user_email" id="materialRegisterFormEmail" class="form-control" value="<?php echo set_value('user_email'); ?>">
                <label for="materialRegisterFormEmail">E-mail</label>
                <span class="text-danger"><?php echo form_error('user_email'); ?></span>
            </div>

            <div class="md-form">
                <input type="password" name="user_password" id="materialRegisterFormPassword" class="form-control" aria-describedby="materialRegisterFormPasswordHelpBlock" value="<?php echo set_value('user_password'); ?>">
                <label for="materialRegisterFormPassword">Password</label>
                <span class="text-danger"><?php echo form_error('user_password'); ?></span>
            </div>

            <button class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit" value="Register">Sign up</button>
            <p>or
                            <a href="<?php echo base_url(); ?>login">Login</a>
                        </p>

        </form>

    </div>

</div>
</div>
<!-- Material form register -->


    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/mdb.min.js"></script>


    <script>
    // assumes you're using jQuery
    $(document).ready(function() {
    $('.verification-flash').hide();
    <?php if($this->session->flashdata('message')){ ?>
    $('.verification-flash').html('<?php echo $this->session->flashdata('message'); ?>').show();
    <?php } ?>
    });
    </script>
</body>
</html>
